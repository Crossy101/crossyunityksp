﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float smooth;
    public float tiltAngle;
    public Rigidbody rb;

    public float velocity;
    public float fuel;
    public float fuelDecrease;
    public float gravity;
    public bool launchedRocket;

	// Use this for initialization
	private void Start () {
        rb.GetComponent<Rigidbody>();
        launchedRocket = false;
	}

    private void Update()
    {
        PlayerLaunchedRocket(); 
    }

    // Update is called once per frame
    private void FixedUpdate () {
        if (launchedRocket)
        {
            //Launch Rocket
            if (fuel > 0)
            {
                if (velocity > 300)
                {
                    velocity = 300;
                }
                else
                {
                    velocity++;
                }
                fuel -= fuelDecrease;
            }
            else
            {
                velocity--;
            }
            rb.AddForce(0, velocity, 0);

            float moveHoriz = Input.GetAxis("Horizontal") * tiltAngle;
            float moveVert = Input.GetAxis("Vertical") * tiltAngle;

            Quaternion target = Quaternion.Euler(moveHoriz, 0, moveVert);

            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
        }
	}

    private void PlayerLaunchedRocket()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            launchedRocket = true;
        }
    }
}
